
USE [TP3]
GO

ALTER TABLE [dbo].[SD_INVENTAIRES]  WITH CHECK ADD  CONSTRAINT [FK_SD_INVENTAIRES_SD_TAILLES] FOREIGN KEY([TAILLES_ID])
REFERENCES [dbo].[SD_TAILLES] ([ID])
GO

ALTER TABLE [dbo].[SD_INVENTAIRES] CHECK CONSTRAINT [FK_SD_INVENTAIRES_SD_TAILLES]
GO

ALTER TABLE SD_PRODUITS
ALTER COLUMN ACTIF BIT NOT NULL;

ALTER TABLE SD_PRODUITS
ALTER COLUMN NON_DISPONIBLE BIT NOT NULL;

ALTER TABLE SD_PRODUITS
ALTER COLUMN MEILLEUR_VENDEUR BIT NOT NULL;

ALTER TABLE SD_PRODUITS
ALTER COLUMN NOUVEAU BIT NOT NULL;

ALTER TABLE SD_PRODUITS
ALTER COLUMN LIQUIDATION BIT NOT NULL;

ALTER TABLE SD_MEMBRES
ALTER COLUMN FIDELITE BIT NOT NULL;

/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4224)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Express Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [TP3]
GO

/****** Object:  Trigger [dbo].[SD_INVENTAIRES_BI_TRG]    Script Date: 2018-12-03 22:26:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[SD_INVENTAIRES_BI_TRG]
   ON  [dbo].[SD_INVENTAIRES]
   INSTEAD OF INSERT
AS 
	DECLARE @l_produit_id INT;
	DECLARE @l_categorie_id INT;
	DECLARE @l_couleur_id INT;
	DECLARE @l_taille_id INT;
	DECLARE @l_quantite INT;
	DECLARE @l_seuil_stok INT;
	DECLARE @l_taux_rabais INT;
	DECLARE @l_seq INT;
	DECLARE @l_sku VARCHAR(18);
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	-- On obtient les valeurs ins�r�s.
	SELECT @l_produit_id = PRODUITS_ID,
		@l_couleur_id = COULEURS_ID,
		@l_taille_id = TAILLES_ID,
		@l_quantite = QUANTITE_STOCK,
		@l_seuil_stok = SEUIL_STOCK,
		@l_taux_rabais = TAUX_RABAIS
	FROM  INSERTED;

	--On obtien le id de la cat�gorie � laquelle appartient le produit.
	SELECT @l_categorie_id =  CATEGORIES_ID
	FROM SD_PRODUITS WHERE ID = @l_produit_id;

	--On d�termine la prochaine s�quence selon les sku existants pour ce produit
	SELECT  @l_seq = MAX(SUBSTRING(SKU,13,6)) FROM SD_INVENTAIRES WHERE SUBSTRING(SKU, 1,4) =  RIGHT('0000' + CAST(@l_categorie_id AS VARCHAR),4);

	IF @l_seq IS NOT NULL  
		SET @l_seq += 1;
	ELSE
		SET @l_seq = 1;

	--PRINT 'SEQ : ' + CAST(@l_seq as varchar) + '';

	-- Cr�ation du nouveau num�ro SKU
	SET @l_sku = RIGHT('0000'+ CAST(@l_categorie_id AS varchar),4) + RIGHT('0000'+ CAST(@l_couleur_id AS varchar),4) +  RIGHT('0000'+ CAST(@l_taille_id AS varchar),4) + RIGHT('000000'+ CAST(@l_seq AS varchar),6)
	--PRINT @l_seq

	--PRINT 'SKU : ' + CAST(@l_sku as varchar) + '';

	-- Insertion dans la table
	INSERT INTO SD_INVENTAIRES (PRODUITS_ID,COULEURS_ID,TAILLES_ID,QUANTITE_STOCK,SEUIL_STOCK,TAUX_RABAIS,SKU)
	VALUES (@l_produit_id, @l_couleur_id, @l_taille_id, @l_quantite, @l_seuil_stok, @l_taux_rabais, @l_sku);

	
END
GO

ALTER TABLE [dbo].[SD_INVENTAIRES] ENABLE TRIGGER [SD_INVENTAIRES_BI_TRG]
GO

/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4224)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Express Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [TP3]
GO

/****** Object:  StoredProcedure [dbo].[RECHERCHE_PRC]    Script Date: 2018-12-03 22:28:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RECHERCHE_PRC]
	-- Add the parameters for the stored procedure here
	@i_recherche varchar(255),
	@i_actif bit = 1
AS
BEGIN
	DECLARE @l_recherche varchar(255);
	
	set @l_recherche = '%' + UPPER(@i_recherche) + '%';

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	PRINT @l_recherche;
	
    -- Insert statements for procedure here
	SELECT P.ID, P.CODE, P.NOM, P.DESCRIPTION, I.IMAGE, P.PRIX, P.NON_DISPONIBLE, P.MEILLEUR_VENDEUR, P.LIQUIDATION, P.NOUVEAU  FROM SD_PRODUITS P       
        INNER JOIN SD_MARQUES M ON M.ID = P.MARQUES_ID
        INNER JOIN SD_CATEGORIES C ON C.ID = P.CATEGORIES_ID
        LEFT JOIN SD_IMAGES I ON I.PRODUITS_ID = P.ID
        LEFT JOIN SD_PRODUIT_VARIANTES PV ON PV.PRODUITS_ID = P.ID
        LEFT JOIN SD_VARIANTES V ON PV.VARIANTES_ID = V.ID
        WHERE P.ACTIF = @i_actif AND (UPPER(P.CODE) = @l_recherche OR UPPER(P.NOM) LIKE @l_recherche OR UPPER(M.DESCRIPTION) LIKE @l_recherche OR UPPER(C.NOM) LIKE @l_recherche OR UPPER(V.DESCRIPTION) LIKE @l_recherche)

	RETURN
END
GO



