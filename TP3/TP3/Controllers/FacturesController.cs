﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TP3.Models;

namespace TP3.Controllers
{
    public class FacturesController : Controller
    {
        private Entities1 db = new Entities1();

        // GET: Factures
        public ActionResult Index()
        {
            var sD_FACTURES = db.SD_FACTURES.Include(s => s.SD_MEMBRES).Include(s => s.SD_PROMOTIONS).Include(s => s.SD_PROVINCES);
            return View(sD_FACTURES.ToList());
        }

        // GET: Factures/Details/5
        public ActionResult Details(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SD_FACTURES sD_FACTURES = db.SD_FACTURES.Find(id);
            if (sD_FACTURES == null)
            {
                return HttpNotFound();
            }
            return View(sD_FACTURES);
        }

        // GET: Factures/Create
        public ActionResult Create()
        {
            ViewBag.MEMBRE_ID = new SelectList(db.SD_MEMBRES, "ID", "NOM");
            ViewBag.PROMOTIONS_CODE = new SelectList(db.SD_PROMOTIONS, "CODE", "CODE");
            ViewBag.PROVINCE_CODE_LIVRAISON = new SelectList(db.SD_PROVINCES, "CODE", "NOM");
            return View();
        }

        // POST: Factures/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,MEMBRE_ID,DATE_VENTE,MONTANT_PST,MONTANT_GST_HST,TOTAL_RABAIS,TYPE_PAIEMENT,PROMOTIONS_CODE,NOM_LIVRAISON,PRENOM_LIVRAISON,ADRESSE_LIVRAISON,VILLE_LIVRAISON,PROVINCE_CODE_LIVRAISON,CODE_POSTAL_LIVRAISON,STATUT,TOTAL_LIVRAISON,TOTAL_TAXES,SOUS_TOTAL,TOTAL")] SD_FACTURES sD_FACTURES)
        {
            if (ModelState.IsValid)
            {
                db.SD_FACTURES.Add(sD_FACTURES);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MEMBRE_ID = new SelectList(db.SD_MEMBRES, "ID", "NOM", sD_FACTURES.MEMBRE_ID);
            ViewBag.PROMOTIONS_CODE = new SelectList(db.SD_PROMOTIONS, "CODE", "CODE", sD_FACTURES.PROMOTIONS_CODE);
            ViewBag.PROVINCE_CODE_LIVRAISON = new SelectList(db.SD_PROVINCES, "CODE", "NOM", sD_FACTURES.PROVINCE_CODE_LIVRAISON);
            return View(sD_FACTURES);
        }

        // GET: Factures/Edit/5
        public ActionResult Edit(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SD_FACTURES sD_FACTURES = db.SD_FACTURES.Find(id);
            if (sD_FACTURES == null)
            {
                return HttpNotFound();
            }
            ViewBag.MEMBRE_ID = new SelectList(db.SD_MEMBRES, "ID", "NOM", sD_FACTURES.MEMBRE_ID);
            ViewBag.PROMOTIONS_CODE = new SelectList(db.SD_PROMOTIONS, "CODE", "CODE", sD_FACTURES.PROMOTIONS_CODE);
            ViewBag.PROVINCE_CODE_LIVRAISON = new SelectList(db.SD_PROVINCES, "CODE", "NOM", sD_FACTURES.PROVINCE_CODE_LIVRAISON);
            return View(sD_FACTURES);
        }

        // POST: Factures/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,MEMBRE_ID,DATE_VENTE,MONTANT_PST,MONTANT_GST_HST,TOTAL_RABAIS,TYPE_PAIEMENT,PROMOTIONS_CODE,NOM_LIVRAISON,PRENOM_LIVRAISON,ADRESSE_LIVRAISON,VILLE_LIVRAISON,PROVINCE_CODE_LIVRAISON,CODE_POSTAL_LIVRAISON,STATUT,TOTAL_LIVRAISON,TOTAL_TAXES,SOUS_TOTAL,TOTAL")] SD_FACTURES sD_FACTURES)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sD_FACTURES).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MEMBRE_ID = new SelectList(db.SD_MEMBRES, "ID", "NOM", sD_FACTURES.MEMBRE_ID);
            ViewBag.PROMOTIONS_CODE = new SelectList(db.SD_PROMOTIONS, "CODE", "CODE", sD_FACTURES.PROMOTIONS_CODE);
            ViewBag.PROVINCE_CODE_LIVRAISON = new SelectList(db.SD_PROVINCES, "CODE", "NOM", sD_FACTURES.PROVINCE_CODE_LIVRAISON);
            return View(sD_FACTURES);
        }

        // GET: Factures/Delete/5
        public ActionResult Delete(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SD_FACTURES sD_FACTURES = db.SD_FACTURES.Find(id);
            if (sD_FACTURES == null)
            {
                return HttpNotFound();
            }
            return View(sD_FACTURES);
        }

        // POST: Factures/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(decimal id)
        {
            SD_FACTURES sD_FACTURES = db.SD_FACTURES.Find(id);
            db.SD_FACTURES.Remove(sD_FACTURES);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
