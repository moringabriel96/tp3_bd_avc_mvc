﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TP3.Models;

namespace TP3.Controllers
{
    public class MembresController : Controller
    {
        private Entities1 db = new Entities1();
        private ApplicationUserManager manager = new ApplicationUserManager(new UserStore<ApplicationUser>(new ApplicationDbContext()));
        // GET: Membres
        public ActionResult Index()
        {
            var sD_MEMBRES = db.SD_MEMBRES.Include(s => s.SD_PROVINCES);
            return View(sD_MEMBRES.ToList());
        }

        // GET: Membres/Details/5
        public ActionResult Details(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SD_MEMBRES sD_MEMBRES = db.SD_MEMBRES.Find(id);
            if (sD_MEMBRES == null)
            {
                return HttpNotFound();
            }
            return View(sD_MEMBRES);
        }

        // GET: Membres/Create
        public ActionResult Create()
        {
            ViewBag.PROVINCES_CODE = new SelectList(db.SD_PROVINCES, "CODE", "NOM");
            return View();
        }
        public async System.Threading.Tasks.Task<ActionResult> SpecialCreateAsync(SD_MEMBRES sD_MEMBRES)
        {
            ApplicationUser newUser = null;
            string useremail = TempData["userEmail"].ToString();
            string userPass = TempData["userPass"].ToString();
            TempData["userEmail"] = ""; //Pour éviter que les informations persistent dans la cache par la suite.
            TempData["userPass"] = "";
            if (useremail != null && userPass != null)
                newUser = new ApplicationUser { UserName = useremail, Email = useremail };
            else
                return View("Error");

            if (ModelState.IsValid && newUser != null) //Le model est valide et nous avons cree un ApplicationUser 
            {
                var result = await manager.CreateAsync(newUser, userPass);
                if (result.Succeeded)
                {
                    string membre = "Membre";
                    await manager.AddToRoleAsync(newUser.Id, membre);
                    db.SD_MEMBRES.Add(sD_MEMBRES);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                    return View("Error");
            }

            ViewBag.PROVINCES_CODE = new SelectList(db.SD_PROVINCES, "CODE", "NOM", sD_MEMBRES.PROVINCES_CODE);
            return View(sD_MEMBRES);
        }
        // POST: Membres/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,NOM,PRENOM,NOM_COMPAGNIE,ADRESSE,VILLE,PROVINCES_CODE,CODE_POSTAL,COURRIEL,TELEPHONE,GENRE")] SD_MEMBRES sD_MEMBRES)
        {
            if (ModelState.IsValid)
            {
                db.SD_MEMBRES.Add(sD_MEMBRES);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PROVINCES_CODE = new SelectList(db.SD_PROVINCES, "CODE", "NOM", sD_MEMBRES.PROVINCES_CODE);
            return View(sD_MEMBRES);
        }

        // GET: Membres/Edit/5
        public ActionResult Edit(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SD_MEMBRES sD_MEMBRES = db.SD_MEMBRES.Find(id);
            if (sD_MEMBRES == null)
            {
                return HttpNotFound();
            }
            ViewBag.PROVINCES_CODE = new SelectList(db.SD_PROVINCES, "CODE", "NOM", sD_MEMBRES.PROVINCES_CODE);
            return View(sD_MEMBRES);
        }

        // POST: Membres/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,NOM,PRENOM,NOM_COMPAGNIE,ADRESSE,VILLE,PROVINCES_CODE,CODE_POSTAL,COURRIEL,TELEPHONE,GENRE")] SD_MEMBRES sD_MEMBRES)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sD_MEMBRES).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PROVINCES_CODE = new SelectList(db.SD_PROVINCES, "CODE", "NOM", sD_MEMBRES.PROVINCES_CODE);
            return View(sD_MEMBRES);
        }
        [Authorize(Roles = "Admin")]
        // GET: Membres/Delete/5
        public ActionResult Delete(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SD_MEMBRES sD_MEMBRES = db.SD_MEMBRES.Find(id);
            if (sD_MEMBRES == null)
            {
                return HttpNotFound();
            }
            return View(sD_MEMBRES);
        }
        [Authorize(Roles = "Admin")]
        // POST: Membres/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(decimal id)
        {
            SD_MEMBRES sD_MEMBRES = db.SD_MEMBRES.Find(id);
            db.SD_MEMBRES.Remove(sD_MEMBRES);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
