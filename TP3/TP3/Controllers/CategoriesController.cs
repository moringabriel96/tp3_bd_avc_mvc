﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TP3.Models;

namespace TP3.Controllers
{
    public class CategoriesController : Controller
    {
        private Entities1 db = new Entities1();

        // GET: Categories
        public ActionResult Index()
        {
            var sD_CATEGORIES = db.SD_CATEGORIES.Include(s => s.SD_CATEGORIES2);
            return View(sD_CATEGORIES.ToList());
        }

        // GET: Categories/Details/5
        public ActionResult Details(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SD_CATEGORIES sD_CATEGORIES = db.SD_CATEGORIES.Find(id);
            if (sD_CATEGORIES == null)
            {
                return HttpNotFound();
            }
            return View(sD_CATEGORIES);
        }
        [Authorize(Roles = "Admin")]
        // GET: Categories/Create
        public ActionResult Create()
        {
            ViewBag.CATEGORIES_ID = new SelectList(db.SD_CATEGORIES, "ID", "NOM");
            return View();
        }
        [Authorize(Roles = "Admin")]
        // POST: Categories/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,NOM,CATEGORIES_ID")] SD_CATEGORIES sD_CATEGORIES)
        {
            if (ModelState.IsValid)
            {
                db.SD_CATEGORIES.Add(sD_CATEGORIES);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CATEGORIES_ID = new SelectList(db.SD_CATEGORIES, "ID", "NOM", sD_CATEGORIES.CATEGORIES_ID);
            return View(sD_CATEGORIES);
        }
        [Authorize(Roles = "Admin")]
        // GET: Categories/Edit/5
        public ActionResult Edit(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SD_CATEGORIES sD_CATEGORIES = db.SD_CATEGORIES.Find(id);
            if (sD_CATEGORIES == null)
            {
                return HttpNotFound();
            }
            ViewBag.CATEGORIES_ID = new SelectList(db.SD_CATEGORIES, "ID", "NOM", sD_CATEGORIES.CATEGORIES_ID);
            return View(sD_CATEGORIES);
        }
        [Authorize(Roles = "Admin")]
        // POST: Categories/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,NOM,CATEGORIES_ID")] SD_CATEGORIES sD_CATEGORIES)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sD_CATEGORIES).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CATEGORIES_ID = new SelectList(db.SD_CATEGORIES, "ID", "NOM", sD_CATEGORIES.CATEGORIES_ID);
            return View(sD_CATEGORIES);
        }
        [Authorize(Roles = "Admin")]
        // GET: Categories/Delete/5
        public ActionResult Delete(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SD_CATEGORIES sD_CATEGORIES = db.SD_CATEGORIES.Find(id);
            if (sD_CATEGORIES == null)
            {
                return HttpNotFound();
            }
            return View(sD_CATEGORIES);
        }
        [Authorize(Roles = "Admin")]
        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(decimal id)
        {
            SD_CATEGORIES sD_CATEGORIES = db.SD_CATEGORIES.Find(id);
            db.SD_CATEGORIES.Remove(sD_CATEGORIES);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
