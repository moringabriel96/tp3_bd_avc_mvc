﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TP3.Models;

namespace TP3.Controllers
{
    public class HomeController : Controller
    {
        public PartialViewResult SideNav()
        {
            using (var query = new Entities1())
            {
                List<SD_CATEGORIES> categories = query.Database.SqlQuery<SD_CATEGORIES>("SELECT ID,NOM,CATEGORIES_ID FROM Entities1.SD_CATEGORIES").ToList();
                return PartialView("_LstCategorie",categories);
            }
        }

        public ActionResult Index()
        {
            using (var queries = new Entities1())
            {
                List<SD_PRODUITS> nouvProd = queries.Database.SqlQuery<SD_PRODUITS>("SELECT ID,CODE,NOM,DESCRIPTION,PRIX,MARQUES_ID,NON_DISPONIBLE,ACTIF,MEILLEUR_VENDEUR,LIQUIDATION,NOUVEAU,STYLE FROM SD_PRODUITS WHERE SD_PRODUITS.NOUVEAU = 1")
                    .Take(4)
                    .ToList();
                List<SD_PRODUITS> meilleurVendeur = queries.Database.SqlQuery<SD_PRODUITS>("SELECT ID,CODE,NOM,DESCRIPTION,PRIX,MARQUES_ID,NON_DISPONIBLE,ACTIF,MEILLEUR_VENDEUR,LIQUIDATION,NOUVEAU,STYLE FROM SD_PRODUITS WHERE SD_PRODUITS.MEILLEUR_VENDEUR = 1")
                    .Take(4)
                    .ToList();
                List<SD_PRODUITS> liquidation = queries.Database.SqlQuery<SD_PRODUITS>("SELECT ID,CODE,NOM,DESCRIPTION,PRIX,MARQUES_ID,NON_DISPONIBLE,ACTIF,MEILLEUR_VENDEUR,LIQUIDATION,NOUVEAU,STYLE FROM SD_PRODUITS WHERE SD_PRODUITS.LIQUIDATION = 1")
                    .Take(4)
                    .ToList();
                if (nouvProd == null || meilleurVendeur == null || liquidation == null)
                {
                    return View();
                }
                else {
                    //Allons chercher les images
                    List<SD_PRODUITS> lstprod = nouvProd.Concat(meilleurVendeur).ToList();
                    lstprod = lstprod.Concat(liquidation).ToList();
                    List<SD_IMAGES> lstimg = new List<SD_IMAGES>();
                    foreach (SD_PRODUITS prod in lstprod)
                    {
                        OracleParameter p = new OracleParameter("prodID", prod.ID.ToString());
                        object[] parameters = new object[] { p };
                        SD_IMAGES img = queries.Database.SqlQuery<SD_IMAGES>("SELECT PRODUITS_ID,COULEURS_ID,IMAGE FROM SD_IMAGES WHERE SD_IMAGES.PRODUITS_ID=:prodID", parameters).ToList().FirstOrDefault();
                        lstimg.Add(img);
                    }
                    ViewBag.lstIMG = lstimg;
                    return View(lstprod.OrderBy(a => a.NOM));
                }
            }
        }


        public ActionResult About()
        {
            ViewBag.Message = "TP3 MVC ASP.NET.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Gabriel Morin Jeremy Lauze.";

            return View();
        }
    }
}