﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TP3.Models;

namespace TP3.Controllers
{
    public class ProduitsController : Controller
    {
        private Entities1 db = new Entities1();

        // GET: Produits/5
        public ActionResult Index(string strRecherche)
        {
            if (strRecherche == null)
            {
                var sD_PRODUITS = db.SD_PRODUITS.Include(s => s.SD_MARQUES);
                return View(sD_PRODUITS.ToList());
            }
            else
            {
                strRecherche = strRecherche.ToLower();
                using (Entities1 context = new Entities1())
                {
                    var query = from prod in db.SD_PRODUITS
                                join prodCat in db.SD_PRODUIT_CATEGORIES on prod.ID equals prodCat.PRODUITS_ID
                                join cat in db.SD_CATEGORIES on prodCat.CATEGORIES_ID equals cat.ID
                                join mar in db.SD_MARQUES on prod.MARQUES_ID equals mar.ID
                                join typeVarCat in db.SD_TYPE_VARIANTE_CATEGORIES on cat.ID equals typeVarCat.CATEGORIES_ID
                                join typeVar in db.SD_TYPE_VARIANTES on typeVarCat.TYPE_VARIANTES_ID equals typeVar.ID
                                where (prod.CODE.ToLower().Contains(strRecherche) || prod.NOM.ToLower().Contains(strRecherche) || cat.NOM.ToLower().Contains(strRecherche)
                                || mar.NOM.ToLower().Contains(strRecherche) || typeVar.NOM.ToLower().Contains(strRecherche))
                                orderby (cat.NOM)
                                orderby (prod.NOM)
                                select prod;

                    var lstResultats = query.ToList();
                    return View(lstResultats);
                }
            }
        }

        // GET: Produits/Details/5
        public ActionResult Details(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SD_PRODUITS sD_PRODUITS = db.SD_PRODUITS.Find(id);
            if (sD_PRODUITS == null)
            {
                return HttpNotFound();
            }
            return View(sD_PRODUITS);
        }
        [Authorize(Roles = "Admin")]
        // GET: Produits/Create
        public ActionResult Create()
        {
            ViewBag.MARQUES_ID = new SelectList(db.SD_MARQUES, "ID", "NOM");
            return View();
        }

        // POST: Produits/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,CODE,NOM,DESCRIPTION,PRIX,MARQUES_ID,NON_DISPONIBLE,ACTIF,MEILLEUR_VENDEUR,LIQUIDATION,NOUVEAU,STYLE")] SD_PRODUITS sD_PRODUITS)
        {
            if (ModelState.IsValid)
            {
                db.SD_PRODUITS.Add(sD_PRODUITS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MARQUES_ID = new SelectList(db.SD_MARQUES, "ID", "NOM", sD_PRODUITS.MARQUES_ID);
            return View(sD_PRODUITS);
        }
        [Authorize(Roles = "Admin")]
        // GET: Produits/Edit/5
        public ActionResult Edit(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SD_PRODUITS sD_PRODUITS = db.SD_PRODUITS.Find(id);
            if (sD_PRODUITS == null)
            {
                return HttpNotFound();
            }
            ViewBag.MARQUES_ID = new SelectList(db.SD_MARQUES, "ID", "NOM", sD_PRODUITS.MARQUES_ID);
            return View(sD_PRODUITS);
        }

        // POST: Produits/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,CODE,NOM,DESCRIPTION,PRIX,MARQUES_ID,NON_DISPONIBLE,ACTIF,MEILLEUR_VENDEUR,LIQUIDATION,NOUVEAU,STYLE")] SD_PRODUITS sD_PRODUITS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sD_PRODUITS).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MARQUES_ID = new SelectList(db.SD_MARQUES, "ID", "NOM", sD_PRODUITS.MARQUES_ID);
            return View(sD_PRODUITS);
        }

        // GET: Produits/Delete/5
        [Authorize(Roles ="Admin")]
        public ActionResult Delete(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SD_PRODUITS sD_PRODUITS = db.SD_PRODUITS.Find(id);
            if (sD_PRODUITS == null)
            {
                return HttpNotFound();
            }
            return View(sD_PRODUITS);
        }

        // POST: Produits/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(decimal id)
        {
            SD_PRODUITS sD_PRODUITS = db.SD_PRODUITS.Find(id);
            db.SD_PRODUITS.Remove(sD_PRODUITS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
