﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace TP3.Models
{
    // Vous pouvez ajouter des données de profil pour l'utilisateur en ajoutant d'autres propriétés à votre classe ApplicationUser. Pour en savoir plus, consultez https://go.microsoft.com/fwlink/?LinkID=317594.
    public class ApplicationUser : IdentityUser
    {

        public decimal MembreId { get; set; }
         
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Notez qu'authenticationType doit correspondre à l'élément défini dans CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Ajouter les revendications personnalisées de l’utilisateur ici
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("OracleDbContext", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("C##TP3"); //letter majuscule
            modelBuilder.Entity<ApplicationUser>()
            .ToTable("ASPNETUSERS").Property(p => p.UserName).HasColumnName("USERNAME");
            modelBuilder.Entity<ApplicationUser>()
            .ToTable("ASPNETUSERS").Property(p => p.AccessFailedCount).HasColumnName("ACCESSFAILEDCOUNT");
            modelBuilder.Entity<ApplicationUser>()
            .ToTable("ASPNETUSERS").Property(p => p.LockoutEnabled).HasColumnName("LOCKOUTENABLED");
            modelBuilder.Entity<ApplicationUser>()
            .ToTable("ASPNETUSERS").Property(p => p.LockoutEndDateUtc).HasColumnName("LOCKOUTENDDATEUTC");
            modelBuilder.Entity<ApplicationUser>()
            .ToTable("ASPNETUSERS").Property(p => p.TwoFactorEnabled).HasColumnName("TWOFACTORENABLED");
            modelBuilder.Entity<ApplicationUser>()
            .ToTable("ASPNETUSERS").Property(p => p.PhoneNumberConfirmed).HasColumnName("PHONENUMBERCONFIRMED");
            modelBuilder.Entity<ApplicationUser>()
            .ToTable("ASPNETUSERS").Property(p => p.PhoneNumber).HasColumnName("PHONENUMBER");
            modelBuilder.Entity<ApplicationUser>()
            .ToTable("ASPNETUSERS").Property(p => p.SecurityStamp).HasColumnName("SECURITYSTAMP");
            modelBuilder.Entity<ApplicationUser>()
            .ToTable("ASPNETUSERS").Property(p => p.PasswordHash).HasColumnName("PASSWORDHASH");
            modelBuilder.Entity<ApplicationUser>()
            .ToTable("ASPNETUSERS").Property(p => p.EmailConfirmed).HasColumnName("EMAILCONFIRMED");
            modelBuilder.Entity<ApplicationUser>()
            .ToTable("ASPNETUSERS").Property(p => p.Email).HasColumnName("EMAIL");
            modelBuilder.Entity<ApplicationUser>()
            .ToTable("ASPNETUSERS").Property(p => p.Id).HasColumnName("ID");
            modelBuilder.Entity<IdentityUserRole>()
            .ToTable("ASPNETUSERROLES").Property(p => p.RoleId).HasColumnName("ROLEID");
            modelBuilder.Entity<IdentityUserRole>()
            .ToTable("ASPNETUSERROLES").Property(p => p.UserId).HasColumnName("USERID");
            modelBuilder.Entity<IdentityUserLogin>()
            .ToTable("ASPNETUSERLOGINS").Property(p => p.UserId).HasColumnName("USERID");
            modelBuilder.Entity<IdentityUserLogin>()
            .ToTable("ASPNETUSERLOGINS").Property(p => p.ProviderKey).HasColumnName("PROVIDERKEY");
            modelBuilder.Entity<IdentityUserLogin>()
            .ToTable("ASPNETUSERLOGINS").Property(p => p.LoginProvider).HasColumnName("LOGINPROVIDER");
            modelBuilder.Entity<IdentityUserClaim>()
            .ToTable("ASPNETUSERCLAIMS").Property(p => p.Id).HasColumnName("ID");
            modelBuilder.Entity<IdentityUserClaim>()
            .ToTable("ASPNETUSERCLAIMS").Property(p => p.UserId).HasColumnName("USERID");
            modelBuilder.Entity<IdentityUserClaim>()
            .ToTable("ASPNETUSERCLAIMS").Property(p => p.ClaimType).HasColumnName("CLAIMTYPE");
            modelBuilder.Entity<IdentityUserClaim>()
            .ToTable("ASPNETUSERCLAIMS").Property(p => p.ClaimValue).HasColumnName("CLAIMVALUE");
            modelBuilder.Entity<IdentityRole>()
            .ToTable("ASPNETROLES").Property(p => p.Id).HasColumnName("ID");
            modelBuilder.Entity<IdentityRole>()
            .ToTable("ASPNETROLES").Property(p => p.Name).HasColumnName("NAME");
            modelBuilder.Entity<ApplicationUser>()
            .ToTable("ASPNETUSERS").Property(p => p.MembreId).HasColumnName("MEMBRE_ID");
        }
    }
}